#!/usr/bin/python2.7

import textfsm
import os

# Read all files in a directory
basepath = '/Volumes/DATA/Documents/0.Work/Project/id-smartfren/POC/2017/script2/'
for filename in os.listdir(basepath):
    path = os.path.join(basepath,filename)
    if os.path.isdir(path):
        continue
#for filename in os.listdir(os.getcwd()):
#input_file = open("COR_BII_0202")
    input_file = open(path)
    raw_text_data = input_file.read()
    input_file.close()

# Run the text through the FSM. 
# The argument 'template' is a file handle and 'raw_text_data' is a 
# string with the content from input file
    template = open("optics.textfsm")
    re_table = textfsm.TextFSM(template)
    fsm_results = re_table.ParseText(raw_text_data)

# the results are written (appended) to a CSV file
 #outfile_name = open("outfile.csv", "w+")
    outfile_name = open("outfile.csv", "a")
    outfile = outfile_name

# Display result as CSV and write it to the output file
# First the column headers...
    print(re_table.header)
    for s in re_table.header:
       outfile.write("%s," % s)
    outfile.write("\n")

# ...now all row's which were parsed by TextFSM
    counter = 0
    for row in fsm_results:
        print(row)
        for s in row:
            outfile.write("%s," % s)
        outfile.write("\n")
        counter += 1
    print("Write %d records" % counter)
